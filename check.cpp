#include <fstream>
#include "check.hpp"

using namespace std;

bool check_input_args(int argc, char **argv)
{
	if (argc != 3)
		return false;
	if (!ifstream(argv[1]))
		return false;
	if(!ofstream(argv[2]))
		return false;
	return true;
}

