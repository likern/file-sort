#ifndef _CHECK_HPP_
#define _CHECK_HPP_

/**
 * @brief Checks that input arguments are valid.
 * @retval true arguments are correct
 * @retval false arguments are not correct
 */
bool check_input_args(int argc, char **argv);

#endif
