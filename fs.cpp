#include "fs.hpp"

using namespace boost::filesystem;
using namespace std;

FileDeleter::~FileDeleter()
{
    for (auto &i : files)
    {
        cerr << "Deleting file [" << i.string() << "]"<< endl;
        remove(i);
    }
}

void FileDeleter::AppendFile(path &file)
{
    files.push_back(file);
}

Filesystem::Filesystem() : gen(std::random_device()())
{}

Filesystem::Filesystem(vector<boost::filesystem::path> &files_):
    files(files_), gen(std::random_device()())
{}

Filesystem::~Filesystem()
{
    for(auto &i : files)
    {
        cerr << "Deleting temporary file: [" << i << "]" << endl;
        remove(i);
    }
}

path Filesystem::CreateUniqueFile(const path &dir)
{
    if(!is_directory(dir))
    {
        throw runtime_error("Is not directory: " + dir.string());
    }

    path temp_file_name;
    do {
        temp_file_name = dir;
        uint64_t val = dis(gen);
        absolute(temp_file_name /= path(to_string(val) + ".tmp"));
    } while (!exists(temp_file_name));
    path file_name = temp_file_name;
    boost::filesystem::fstream tmp(file_name);
    cerr << "Created file: " << file_name << endl;
    files.push_back(file_name);
    return file_name;
}

path Filesystem::CreateUniqueTmpFile()
{
    return CreateUniqueFile(temp_directory_path());
}

path Filesystem::GetNonExistingFile()
{
    path dir = temp_directory_path();
    path temp_file_name;
    do {
        temp_file_name = dir;
        uint64_t val = dis(gen);
        absolute(temp_file_name /= path(to_string(val) + ".tmp"));
    } while (exists(temp_file_name));
    cerr << "Non existing file path: " << temp_file_name << endl;
    return temp_file_name;
}
