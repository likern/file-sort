#ifndef _FILESYSTEM_HPP_
#define _FILESYSTEM_HPP_

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <vector>
#include <random>

/**
 * @brief Automatically delete files, when object
 * out-of-scope
 */

class FileDeleter final
{
public:
    FileDeleter() = default;
    FileDeleter(const FileDeleter&) = delete;
    FileDeleter &operator=(const FileDeleter&) = delete;
    ~FileDeleter();
    void AppendFile(boost::filesystem::path &file);
private:
    std::vector<boost::filesystem::path> files;
};

/**
 * @brief Gives ability to create really unique files,
 * to use in unit tests, where file passes as command line
 * arguments. Now is not used in code.
 */
class Filesystem final
{
public:
    Filesystem();

    /**
     * @param[in] files Pass files ownership to object.
     * Then these files will be deleted after object deletion.
     */
    Filesystem(std::vector<boost::filesystem::path> &files_);
    ~Filesystem();
    boost::filesystem::path CreateUniqueFile(const boost::filesystem::path &dir);
    boost::filesystem::path CreateUniqueTmpFile();
    boost::filesystem::path GetNonExistingFile();

private:
    std::vector<boost::filesystem::path> files;
    std::mt19937 gen;
    std::uniform_int_distribution<uint64_t> dis;
};

#endif
