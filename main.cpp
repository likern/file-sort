#include <ostream>
#include <future>
#include "check.hpp"
#include "manager.hpp"
#include "thread_job.hpp"
#include "thread_pool.hpp"
#include "multi_merge.hpp"

using namespace std;

int main(int argc, char **argv)
{
    try {
        if(!check_input_args(argc, argv))
        {
            cout << "Invalid input arguments" << endl;
            cout << "artec-sort <unsorted.file> <result.file>" << endl;
            return EXIT_FAILURE;
        }

        DataManager mgr(argv[1], DataManager::DEFAULT_ELEMENTS);
        thread_pool pool;
        vector<future<boost::filesystem::path>> futures;
        boost::filesystem::path input_file(argv[1]);

        // Add every unsorted chunk of data to pool
        // Pool will them sortm write to tmp file and
        // return this tmp file path

        DataChunk chunk;
        while (chunk = mgr.next_chunk())
        {
            futures.push_back(
                pool.submit(ThreadTask(chunk, &input_file)));
        }

        FileDeleter scoped_files;
        vector<boost::filesystem::path> results;
        for (auto &i : futures)
        {
            auto tmp = i.get();
            scoped_files.AppendFile(tmp);
            results.push_back(tmp);
        }

        //TEST
        for (auto &i : results)
        {
            cout << "File:" << i << endl;
        }

        boost::filesystem::path output(argv[2]);
        MultiMerge kmerge(output, results);
        kmerge.run();
        cout << "Successfully sorted final file: " << output << endl;
    } catch (std::exception &err) {
        cerr << "Unknown error: " << err.what() << endl;
        return EXIT_FAILURE;
    } catch (...) {
        cerr << "Unknown error: No description." << endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
