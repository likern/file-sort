#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <thread>
#include "manager.hpp"

using namespace std;
using namespace boost::filesystem;

DataManager::DataManager(const path &file, size_t elements_) :
    curr_pos(0), segment(elements_ * sizeof(uint32_t)), file_(file)
{
    file_size = get_file_size();
    if (file_size % sizeof(uint32_t))
    {
        string err = string("File contains [") + to_string(file_size) + "] bytes."
                + "Is not integral number of 32-bit integers";
        cerr << err << endl;
        throw std::runtime_error(err);
    }
    file_size /= sizeof(uint32_t);
    left_size = file_size;
}

DataChunk DataManager::next_chunk()
{
    size_t real_segment = left_size < segment ? left_size : segment;
    DataChunk chunk(curr_pos, real_segment);
    left_size -= real_segment;
    curr_pos += real_segment;
    return chunk;
}

const off64_t DataManager::get_file_size()
{
    errno = 0;
    struct stat64 info;
    int res = stat64(file_.c_str(), &info);
    if (res)
    {
        throw std::runtime_error(strerror(errno));
    }
    cerr << "Size of [" << file_.string() << "] "
         << info.st_size << " bytes" << endl;
    return info.st_size;
}
