#ifndef _MANAGER_HPP_
#define _MANAGER_HPP_

#include <vector>
#include <cstdint>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

/**
 * @brief Describes position and size of data
 * in original file, which can be safely read and
 * sorted.
 */
struct DataChunk final
{
    DataChunk(): pos(0), size(0) {};
    DataChunk(size_t pos_, size_t size_):
        pos(pos_), size(size_) {};
    /**
     * @brief Checks is object is in valid state
     * @retval true object has non-zero data size [not empty]
     * @retval false object has zero data size [empty]
     */
    explicit operator bool() { return !empty(); }
    bool empty() const { return !size; };
    size_t pos;
    size_t size;
};

/**
 * @brief Handles which and how sort data.
 * It splits input file into chunks which later will be passed
 * to threads.
 */
class DataManager final
{
public:
    DataManager(const boost::filesystem::path &file, size_t elements_);
	DataManager(const DataManager&) = delete;
	DataManager &operator=(const DataManager&) = delete;
    enum DefSize { DEFAULT_ELEMENTS = 10}; // 4Mib
    DataChunk next_chunk();
private:
    size_t curr_pos;
    const size_t segment;
    boost::filesystem::path file_;
    off64_t file_size;
    size_t left_size;
    const off64_t get_file_size();
};

#endif
