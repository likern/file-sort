#include "multi_merge.hpp"

MultiMerge::MultiMerge(const boost::filesystem::path &output,
           std::vector<boost::filesystem::path> &files):
    out_file(output, std::ios::app | std::ios::binary), out_path(output)
{
    // Read from every file minimal value to construct
    // min heap based on it.
    for (auto &i : files)
    {
        auto stream_ptr = open_stream(i);
        add_minval_to_min_queue(stream_ptr);
    }
};

MultiMerge::~MultiMerge()
{   
    MinVal min_val;
    while (!min_queue.empty())
    {
        min_val = min_queue.top();
        min_val.file->close();
        min_queue.pop();
    }
}

bool MultiMerge::read_uint32t(uint32_t &num, boost::filesystem::ifstream *file)
{
    bool has_eof = false;
    if (!file->read(reinterpret_cast<char*>(&num), sizeof(uint32_t)))
    {
        if (file->eof() && !file->bad())
        {
            std::cerr << "Get End-Of-File for this stream. Delete from list" << std::endl;
            has_eof = true;
        } else {
            throw std::runtime_error("Read operation failed");
        }
    }

    auto delivered = file->gcount();
    if (delivered % sizeof(uint32_t))
    {
        throw std::runtime_error("Can't read exactly 4 bytes from file");
    }

    if (!delivered && has_eof)
    {
        return false;
    } else {
        throw std::runtime_error("Unexpected error");
    }
    return true;
}

boost::filesystem::ifstream *MultiMerge::open_stream(boost::filesystem::path &file)
{
    auto stream_ptr = new boost::filesystem::ifstream(file, std::ios::binary);
    if (!stream_ptr)
    {
        throw std::runtime_error("Can't open stream file");
    }
    return stream_ptr;
}

void MultiMerge::add_minval_to_min_queue(boost::filesystem::ifstream *file)
{
    MinVal min_val;
    if (read_uint32t(min_val.value, file))
    {
        min_val.file = file;
    } else {
        file->close();
    }
    min_queue.push(min_val);
}

bool MultiMerge::read_next(MinVal &val)
{
    return read_uint32t(val.value, val.file);
}

void MultiMerge::run()
{
    // 1. Read [block_size] bytes from every stream
    // 2. Add these elements to min heap
    // 3. Pop all elements and save them into output file
    while (!min_queue.empty())
    {
        // Delete min value from queue;
        MinVal min_val = min_queue.top();
        out_file.write(reinterpret_cast<char*>(&min_val.value), sizeof(min_val.value));
        min_queue.pop();

        // Has next element from this file
        if (read_next(min_val))
        {
            min_queue.push(min_val);
        }
        else
        {
            min_val.file->close();
        }
    }
    out_file.flush();
}
