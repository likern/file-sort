#ifndef _MULTI_MERGE_HPP_
#define _MULTI_MERGE_HPP_

#include <vector>
#include <list>
#include <cstdint>
#include <fstream>
#include <ios>
#include <queue>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "fs.hpp"

/**
 * @brief Is used to associate value with file stream.
 * Because we have to add next minimal value from
 * stream when popped min value from min-heap.
 */
struct MinVal
{
    MinVal() : value(0), file(nullptr) {};
    bool operator>(const MinVal &other) const
        { return this->value > other.value; }
    uint32_t value;
    boost::filesystem::ifstream *file;
};

/**
 * @brief Performs k-way merge algorithm, based on
 * min heap to reduce number of comparisons.
 */
class MultiMerge final
{
public:
    /**
     * @brief MultiMerge merge sorted chunks into one file with
     * sorting preserving.
     * @param[in] output will be used to store final sorted data
     * @param[in] files files with sorted data.
     */
    MultiMerge(const boost::filesystem::path &output,
               std::vector<boost::filesystem::path> &files);
    MultiMerge(const MultiMerge&) = delete;
    MultiMerge &operator=(const MultiMerge&) = delete;
    ~MultiMerge();

    /**
     * @brief Perform k-way merge
     */
    void run();

private:
    boost::filesystem::ofstream out_file;
    boost::filesystem::path out_path;
    std::priority_queue<MinVal, std::vector<MinVal>,
                        std::greater<MinVal>> min_queue;
    bool read_uint32t(uint32_t &num, boost::filesystem::ifstream *file);
    bool read_next(MinVal &val);
    void add_minval_to_min_queue(boost::filesystem::ifstream *file);
    boost::filesystem::ifstream *open_stream(boost::filesystem::path &file);
};

#endif
