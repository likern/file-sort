#ifndef _ARGUMENTS_HPP_
#define _ARGUMENTS_HPP_

#include <gtest/gtest.h>
#include <fs.hpp>
#include <boost/filesystem.hpp>

using namespace std;

class InputArgsTest : public ::testing::Test {
public:
    InputArgsTest() : argc_(1)
    {
        argv_[0] = strdup("binary");
    }
    ~InputArgsTest()
    {
        for (int i = 0; i < argc_; ++i)
        { free(argv_[i]); }
    }

    void AppendArgument(const char *str)
    {
        argv_[argc_] = strdup(str);
        ++argc_;
    }

    void AppendExistingFileArgument()
    {
        boost::filesystem::path file = fs.CreateUniqueTmpFile();
        AppendArgument(file.c_str());
    }

    void AppendNonExistingFileArgument()
    {
        boost::filesystem::path file = fs.GetNonExistingFile();
        AppendArgument(file.c_str());
    }

    int argc()
    {
        return argc_;
    }

    char **argv()
    {
        return argv_;
    }

private:
    char* argv_[1024];
    int argc_;
    Filesystem fs;
};

#endif
