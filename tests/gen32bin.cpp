#include <vector>
#include <cstdint>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "utility.hpp"

using namespace std;

bool write_to_file(std::vector<uint32_t> &vec, boost::filesystem::ofstream &stream)
{
    streamsize bytes = vec.size() * sizeof(uint32_t);
    try {
        stream.write(reinterpret_cast<char*>(&vec[0]), bytes);
    } catch (ios_base::failure &err) {
        cout << err.what() << endl;
        return false;
    }
    return true;
}

int main(int argc, char **argv)
{
    try {
        Options opt = util_check_args(argc, argv);

        boost::filesystem::path file(opt.file);
        boost::filesystem::ofstream stream(file);
        try {
            stream.exceptions(boost::filesystem::ofstream::badbit |
                              boost::filesystem::ofstream::eofbit |
                              boost::filesystem::ofstream::failbit);
        } catch (std::exception &err)
        {
            cerr << "Can't open file [" << opt.file << "] for writing" << endl;
            cerr << err.what() << endl;
            exit(EXIT_FAILURE);
        }

        random_device rd;
        mt19937 g(rd());
        vector<uint32_t> vec;
        const uint64_t step = 1000000;

        auto flush_vector_to_file = [&g, &opt, &file]
                (std::vector<uint32_t> &vec, boost::filesystem::ofstream &stream)
        {
            if (!opt.sorted)
            { shuffle(vec.begin(), vec.end(), g); }
            // DEBUG
            //cout << "[START PORTION]" <<  endl;
            //for (auto &i : vec) { cout << i << endl; }
            //cout << "[END PORTION]" <<  endl;
            // END DEBUG
            if (write_to_file(vec, stream))
            { vec.clear(); }
            else
            { stream.close(); boost::filesystem::remove(file); }
        };

        uint64_t count = 0;
        for (uint64_t i = opt.min; i < opt.max; ++i)
        {
            ++count;
            vec.push_back(i);
            if (count == step)
            {
                flush_vector_to_file(vec, stream);
                count = 0;
            }
        }

        // Flush the last portion of data,
        // which could be less than <step>
        if (count) { flush_vector_to_file(vec, stream); }

        //cout << "Vector elements: " << endl;
        //for (auto &i : vec) { cout << i << endl; }
    } catch (std::exception &err) {
        cerr << "Unknown error: " << err.what() << endl;
        return EXIT_FAILURE;
    } catch (...) {
        cerr << "Unknown error: No description." << endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
