#!/usr/bin/env bash

SORT_FILE="/tmp/artec.sort.data"
UNSORT_FILE="/tmp/artec.unsort.data"
RESULT_FILE="/tmp/artec.result.data"

function getmd5sum
{
    md5sum "$1" | awk '{print $1}'
}

if [ ! -f "artec-tests" ]    ||
   [ ! -f "gen32bin-tests" ] ||
   [ ! -f "gen32bin" ];
then
    printf "Didn't find required binaries. Build it.\n";
    cmake . || {
        printf "cmake failed.\n" >&2;
        exit 1;
    }

    make || {
        printf "make failed.\n" >&2;
        exit 1;
    }
fi

printf "Run unit-tests for gen32bin utility.\n";
./gen32bin-tests || {
    printf "[FAIL] gen32bin unit-tests failed\n";
    exit 1;
}

printf "Run unit-tests for artec-sort utility.\n";
./artec-tests || {
    printf "[FAIL] artec-sort unit-tests failed\n";
    exit 1;
}

# Correct value: 268435456
printf "Run artec-sort tests on real data\n";
printf "Create 1Gib file $SORT_FILE with sorted data\n";
./gen32bin sorted 0 30 "$SORT_FILE" || {
    printf "Can't create file. Check you have enough free space.\n";
    exit 1;
}
SORT_MD5SUM="$(getmd5sum $SORT_FILE)"

printf "Create 1Gib file $UNSORT_FILE with unsorted data\n";
./gen32bin unsorted 0 30 "$UNSORT_FILE" || {
    printf "Can't create file. Check you have enough free space.\n";
    exit 1;
}

printf "Run artec-sort\n"
../artec-sort "$UNSORT_FILE" "$RESULT_FILE" || {
    printf "artec-sort failed\n";
    exit 1;
}
RESULT_MD5SUM="$(getmd5sum $RESULT_FILE)"

if [ "$RESULT_MD5SUM" == "$SORT_MD5SUM" ]; then
    printf "[SUCCESS] sorted file and file produced by artec-sort are equal.\n";
    #rm -f /tmp/artec.result.data;
    #rm -f /tmp/artec.sort.data;
    #rm -f /tmp/artec.unsort.data;
    exit 0;
else
    printf "[FAIL] sorted file and file produced by artec-sort are not equal.\n";
    exit 1;
fi;
exit 1;
