#include <check.hpp>
#include <manager.hpp>
#include <queue.hpp>
#include <thread_job.hpp>
#include <thread_pool.hpp>
#include <gtest/gtest.h>

#include "arguments.hpp"

TEST_F(InputArgsTest, TestNullArguments) {
    ASSERT_EQ(false, check_input_args(argc(), argv()));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
