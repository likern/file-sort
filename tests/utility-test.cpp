#include <gtest/gtest.h>
#include "utility.hpp"
#include "arguments.hpp"

TEST_F(InputArgsTest, NullArguments) {
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, OneArgument) {
    AppendArgument("sorted");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, TwoArguments) {
    AppendArgument("sorted");
    AppendArgument("10");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, ThreeArguments) {
    AppendArgument("sorted");
    AppendArgument("10");
    AppendArgument("1024");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, NonIntegerMin1) {
    AppendArgument("sorted");
    AppendArgument("some");
    AppendArgument("1024");
    AppendArgument("test_file.bin");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, NonIntegerMin2) {
    AppendArgument("sorted");
    AppendArgument("9.7");
    AppendArgument("1024");
    AppendArgument("test_file.bin");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, NonIntegerMax1) {
    AppendArgument("sorted");
    AppendArgument("0");
    AppendArgument("some");
    AppendArgument("test_file.bin");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, NonIntegerMax2) {
    AppendArgument("sorted");
    AppendArgument("0");
    AppendArgument("1024.0");
    AppendArgument("test_file.bin");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, InvalidSortArgument) {
    AppendArgument("wrong_text");
    AppendArgument("15");
    AppendArgument("1076");
    AppendArgument("test_file.bin");
    ASSERT_DEATH(util_check_args(argc(), argv()), "");
}

TEST_F(InputArgsTest, Correct) {
    AppendArgument("sorted");
    AppendArgument("15");
    AppendArgument("1076");
    AppendArgument("test_file.bin");
    Options opt = util_check_args(argc(), argv());
    ASSERT_EQ(opt.sorted, true);
    ASSERT_EQ(opt.min, 15);
    ASSERT_EQ(opt.max, 1076);
    ASSERT_EQ(opt.file, "test_file.bin");
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    ::testing::FLAGS_gtest_death_test_style = "threadsafe";
    return RUN_ALL_TESTS();
}
