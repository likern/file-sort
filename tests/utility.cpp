#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include "utility.hpp"

using namespace std;
using boost::numeric_cast;
using boost::numeric::bad_numeric_cast;
using boost::numeric::positive_overflow;
using boost::numeric::negative_overflow;
using boost::lexical_cast;
using boost::bad_lexical_cast;

void print_help()
{
    cerr << "gen32bin <sorted|unsorted> <min> <max> <file>" << endl;
    cerr << "Generate 32bit binary file with values in range [min]-[max]" << endl;
    cerr << "Options:" << endl;
    cerr << "sorted or unsorted - will be values put in sorted manner" << endl;
    cerr << "min - minimum integer from which range starts" << endl;
    cerr << "max - maximum integer from which range ends" << endl;
}

Options util_check_args(int argc, char **argv)
{
    if (argc != 5)
    {
        cerr << "Invalid number of arguments" << endl;
        print_help();
        exit(EXIT_FAILURE);
    }

    Options opt;
    if (string("sorted") == argv[1]) {
        opt.sorted = true;
    } else if (string("unsorted") == argv[1]) {
        opt.sorted = false;
    } else  {
        exit(EXIT_FAILURE);
    }

    try {
        opt.min = numeric_cast<uint64_t> (lexical_cast<uint64_t>(argv[2]));
        opt.max = numeric_cast<uint64_t> (lexical_cast<uint64_t>(argv[3]));
    } catch (bad_lexical_cast &err) {
        cerr << err.what() << endl;
        print_help();
        exit(EXIT_FAILURE);
    } catch (negative_overflow& err) {
        cerr << err.what() << endl;
        print_help();
        exit(EXIT_FAILURE);
    } catch (positive_overflow& err) {
        cerr << err.what() << endl;
        print_help();
        exit(EXIT_FAILURE);
    }

    opt.file = argv[4];
    return opt;
}
