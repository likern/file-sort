#ifndef _UTILITY_HPP_
#define _UTILITY_HPP_

#include <cstdint>
#include <string>

/**
 * @brief Contains all required options,
 * after parsing input arguments
 */
struct Options
{
    Options() : min(0), max(1024), sorted(false) {};
    uint64_t min;
    uint64_t max;
    bool sorted;
    std::string file;
};

/**
 * @brief Checks input arguments for gen32bin utility.
 * If input arguments are invalid - close application.
 */
Options util_check_args(int argc, char **argv);

/**
 * @brief Print help message for gen32bin utility
 */
void print_help();

#endif
