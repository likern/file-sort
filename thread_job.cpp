#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <boost/filesystem/fstream.hpp>
#include <thread>
#include "thread_job.hpp"

using namespace std;
std::atomic<uint32_t> ThreadTask::count(0);

boost::filesystem::path ThreadTask::operator() ()
{
    boost::filesystem::ifstream file(*input, ios::binary);
    if (!file.seekg(chunk.pos, ios::beg))
    {
        std::string err = std::string("File [") + input->c_str() + "] " +
                "can't seek to " + to_string(chunk.pos) + " position";
        throw std::runtime_error(err);
    }
    vector<uint32_t> vec(chunk.size, 0);
    size_t bytes = chunk.size * sizeof(uint32_t);
    if (!file.read(reinterpret_cast<char*>(&vec[0]), bytes))
    {
        if (file.eof() && !file.bad())
        {
            std::string msg = std::string("File [") + input->c_str() + "] " +
                    " get end-of-file";
            std::cerr << msg << std::endl;
        }
        else
        {
            throw std::runtime_error("Unexpected file read error");
        }
    }

    std::sort(vec.begin(), vec.end());
	auto tmp_file = generate_file_name();
    boost::filesystem::ofstream stream(tmp_file, ios::binary);
	stream.exceptions(boost::filesystem::ofstream::badbit  |
					  boost::filesystem::ofstream::failbit |
					  boost::filesystem::ofstream::eofbit);
    stream.write(reinterpret_cast<char*>(&vec[0]), bytes);
    cerr << "Thread [" << std::this_thread::get_id() << "] read ["
         << chunk.pos << "-" << chunk.pos + chunk.size << "] elements of file ["
         << input->c_str() << "] and write to [" << tmp_file.string() << "]" << endl;
    return tmp_file;
}

boost::filesystem::path ThreadTask::generate_file_name()
{
	string file_name;
	do {	
		file_name = string("tmp") + to_string(count) + ".chunk";
		++count;
	} while (boost::filesystem::exists(file_name));
	return boost::filesystem::path(file_name);
}
