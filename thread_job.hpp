#ifndef _THREAD_JOB_HPP_
#define _THREAD_JOB_HPP_

#include <vector>
#include <cstdint>
#include <algorithm>
#include <atomic>
#include <boost/filesystem.hpp>
#include "manager.hpp"

/**
 * @brief Sorts chunks of data and write them to tmp files
 */
class ThreadTask final
{
public:
    /**
     * @param[in] chunk_ data chunk, which will be sorted
     * @param[in] input_ file, from which read data chunk
     */
    ThreadTask(DataChunk &chunk_, boost::filesystem::path *input_):
        input(input_), chunk(chunk_) {};
    ThreadTask &operator=(const ThreadTask&) = delete;

    /**
     * @brief ThreadTask::operator() performs the following task:
     * 1. Open input file and read some data chunk.
     * 2. Sorts this chunk and write them to temporary file.
     * @return file, where sorted data chunk was saved.
     */
    boost::filesystem::path operator() ();
private:
    static std::atomic<uint32_t> count;
    const boost::filesystem::path *input;
    DataChunk chunk;

    /**
     * @brief ThreadTask::generate_file_name generates unique filename
     * @return unique file name, which didn't exist at time of function call
     */
	boost::filesystem::path generate_file_name();
};
#endif
